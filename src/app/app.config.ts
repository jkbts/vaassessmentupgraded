import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../environments/environment';
import { map } from 'rxjs/operators';

@Injectable()
export class AppConfig {
    public value: any = [];
    constructor(private http: HttpClient) {
    }

    public load() {
        return this.http.get('../config' + (environment.production ? '' : '.dev') + '.json').pipe(map(res => res));
    }
}