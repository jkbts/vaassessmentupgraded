import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApiService } from '../services/api.service';

@Component({
    selector: 'app-assessment-landing',
    templateUrl: './assessment-landing.component.html',
    styleUrls: ['./assessment-landing.component.css']
})
export class AssessmentLandingComponent implements OnInit {

    naCompleted: any;
    arCompleted: any;
    totalAR: any;
    totalNA: any;
    
    constructor(private router: Router, private apiService: ApiService) { }

    ngOnInit() {
        this.apiService.loadAbstractReasoningConf().subscribe((config: any) => {
            this.totalAR = config.length;
            this.apiService.loadNumericalAbilityConf().subscribe((result: any) => {
                this.totalNA = result.length;
                this.naCompleted = JSON.parse(localStorage.getItem('naCompletedStatus'));
                this.arCompleted = JSON.parse(localStorage.getItem('arCompletedStatus'));
            });
        });
    }

    startAbstractReasoningAssessment() {
        this.apiService.loadAbstractReasoningConf().subscribe((config: any) => {
            const abstractdata = config;
            const data = config[0];
            
            const navigationExtras: NavigationExtras = {
                state: {
                    data: data
                }
            };

            localStorage.setItem('singleselectdata', JSON.stringify(data));
            localStorage.setItem('abstractdata', JSON.stringify(abstractdata));
            localStorage.setItem('abstractdataindex', '0');
            this.router.navigate(['home/abstract'], navigationExtras);
        });

    }

    startNumericalAbilityAssessment() {
        this.apiService.loadNumericalAbilityConf().subscribe((config: any) => {
            const numericaldata = config;
            const data = config[0];
            
            const navigationExtras: NavigationExtras = {
                state: {
                    data: data
                }
            };

            localStorage.setItem('singleselectnumericaldata', JSON.stringify(data));
            localStorage.setItem('numericaldata', JSON.stringify(numericaldata));
            localStorage.setItem('numericaldataindex', '0');
            this.router.navigate(['home/numerical'], navigationExtras);
        });
    }

    startMemoryAssessment() {
        this.router.navigate(['home/memory/new']);
    }

    navigateToResults() {
        this.router.navigate(['home/dashboard']);
    }

}