import { Component, OnInit, Input, ChangeDetectorRef, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';

@Component({
    selector: 'single-select-image',
    templateUrl: './single-select-image.component.html',
    styleUrls: ['./single-select-image.component.css']
})
export class SingleSelectImageComponent implements OnInit, AfterViewInit {

    @Input() data: any = '';
    public arCompleted: number = 0;
    public naCompleted: number = 0;

    @ViewChild('myTimer', { static: false }) public timerRef;
    public selectedOption: any;
    constructor(private router: Router, private apiService: ApiService, private cdr: ChangeDetectorRef) { }

    ngOnInit() {
        console.log('ss', this.data);
        
        this.apiService.getEmitter('re-render-ssi').subscribe((result: any) => {
            this.data = null;
            this.cdr.detectChanges();
            this.data = result;
            this.cdr.detectChanges();
        });
    }

    ngAfterViewInit() {
        console.log(this.timerRef.timerValue);
    }

    selectOption(option: any) {
        this.selectedOption = option.value;
        for (let i = 0; i < this.data.question.options.length; i++) {
            this.data.question.options[i].selected = false;
        }
        option.selected = true;
    }

    saveVotes() {
        console.log(this.data.question.binding);
        console.log(this.selectedOption);
        console.log(this.timerRef.timerValue);
        this.apiService.get('saveresponse?response=' + this.selectedOption + '&question=' + this.data.question.binding + '').subscribe(x => {
            if (this.data.class === 'ar-ssi') {
                this.arCompleted = JSON.parse(localStorage.getItem('arCompletedStatus')) + 1;
                localStorage.setItem('arCompletedStatus', JSON.stringify(this.arCompleted));
                this.apiService.emitData('navigate-to-ar-next', '');
            }

            if (this.data.class === 'na-ssi') {
                this.naCompleted = JSON.parse(localStorage.getItem('naCompletedStatus')) + 1;
                localStorage.setItem('naCompletedStatus', JSON.stringify(this.naCompleted));
                this.apiService.emitData('navigate-to-na-next', '');
            }
        })
        this.apiService.get('saveresponse?response=' + this.timerRef.timerValue + '&question=' + this.data.question.basicTimerSettings.binding + '').subscribe(x => {
            console.log(x);
        })
    }
}