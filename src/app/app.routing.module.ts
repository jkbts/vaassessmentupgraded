import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LandingComponent } from './landing/landing.component';
import { AssessmentLandingComponent } from './assessment-landing/assessment-landing.component';
import { AbstractReasoningComponent } from './abstract-reasoning/abstract-reasoning.component';
import { MemoryHomeComponent } from './memory/home/home.component';
import { AboutComponent } from './memory/about/about.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NumericalAbilityComponent } from './numerical-ability/numerical-ability.component';
import { RecommendationsComponent } from './recommendations/recommendations.component';
import { Memory2Component } from './memory2/memory2.component';

const routes: Routes = [
    {
        path: 'home', component: HomeComponent,
        children: [
            { path: 'landing', component: LandingComponent },
            { path: 'assessment', component: AssessmentLandingComponent },
            { path: 'dashboard', component: DashboardComponent },
            { path: 'abstract', component: AbstractReasoningComponent },
            { path: 'numerical', component: NumericalAbilityComponent },
            { path: 'memory/new', component: MemoryHomeComponent },
            { path: 'memory/about', component: AboutComponent },
            { path: 'recommendations', component: RecommendationsComponent },
            { path: 'memory2', component: Memory2Component }

        ]
    },
    { path: '', redirectTo: '/login', pathMatch: 'full' },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AppRoutingModule { }
