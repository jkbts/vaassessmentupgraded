import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app.routing.module';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { LandingComponent } from './landing/landing.component';
import { HeaderComponent } from './header/header.component';
import { AssessmentLandingComponent } from './assessment-landing/assessment-landing.component';
import { SingleSelectImageComponent } from './single-select-image/single-select-image.component';
import { AbstractReasoningComponent } from './abstract-reasoning/abstract-reasoning.component';
import { AppConfig } from './app.config';
import { HttpClientModule } from '@angular/common/http';

import {FormsModule} from '@angular/forms';
// import {ShareButtonsModule} from 'ngx-sharebuttons';

import {MemoryBoardComponent} from './memory/memory-board/memory-board.component';
import {MemoryCardComponent} from './memory/memory-card/memory-card.component';
import {MemoryService} from "./memory/memory/memory.service";
import {SharedModule} from "./memory/shared/index";
import {MemoryHomeComponent} from './memory/home/home.component';
import {CardsService} from "./memory/cards/cards.service";
import {StateComponent} from './memory/state/state.component';
import {AboutComponent} from './memory/about/about.component';
import {ResultComponent} from './memory/result/result.component';
import {ResultPipe} from './memory/result/result.pipe';
import {LogoComponent} from './memory/logo/logo.component';
import { TimerComponent } from './memory/shared/timer/timer.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NumericalAbilityComponent } from './numerical-ability/numerical-ability.component';
import { RecommendationsComponent } from './recommendations/recommendations.component';
import { Memory2Component } from './memory2/memory2.component';
// import { HighchartsStatic } from './dashboard/highchartsstatic';

const routes = RouterModule.forRoot([
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  {
    path: 'login', component: LoginComponent
  }
], { useHash: true });

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    LoginComponent,
    LandingComponent,
    HeaderComponent,
    AssessmentLandingComponent,
    SingleSelectImageComponent,
    AbstractReasoningComponent,
    MemoryBoardComponent,
    MemoryCardComponent,
    MemoryHomeComponent,
    StateComponent,
    AboutComponent,
    ResultComponent,
    ResultPipe,
    LogoComponent,
    TimerComponent,
    DashboardComponent,
    NumericalAbilityComponent,
    RecommendationsComponent,
    Memory2Component
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    routes,
    HttpClientModule,
    FormsModule,
    SharedModule,
    // ShareButtonsModule
  ],
  exports: [HomeComponent],
  providers: [AppConfig],
  bootstrap: [AppComponent]
})
export class AppModule { }
