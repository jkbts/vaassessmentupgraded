import { EventEmitter, Injectable } from '@angular/core';
// import { AppConfig } from '../../app.config';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { debounceTime, map, catchError } from 'rxjs/operators';
// import { HttpParams } from '@angular/common/http'
import { Observable, Observer, from, throwError } from 'rxjs';
import * as _ from 'lodash';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    private hostname: string = 'http://localhost:1234/api/';

    public baseUrl: string = 'http://localhost/PulseServices/';

    public eventsUrl: string = 'http://localhost/Wizer/Pages/Events/';

    public wizerServer: string = 'http://localhost/';

    public votesObservable: any;

    public _observableEmitter: any = {};

    constructor(private http: HttpClient) { }

    loadAbstractReasoningConf() {
        return this.http.get('../../assets/json/abstract-reasoning.json');
    }

    loadNumericalAbilityConf() {
        return this.http.get('../../assets/json/numerical-ability.json');
    }

    get(url: string): Observable<any> {
        const headers = new HttpHeaders({
            'Cache-Control': 'no-cache',
            'Pragma': 'no-cache'
        });
        const options = { headers: headers, withCredentials: false};
        return this.http.get(this.hostname +url, options);
    }

    postRequest(url: string, body: any): Observable<any> {
        const headers = new HttpHeaders({
            'Content-Type': 'application/json'
        });
        const options = { headers: headers, withCredentials: true };
        const params = JSON.stringify(body);

        return this.http.post(url, params, options)
            .pipe(map((data) => data),
                catchError((err) => this.handleError(err)));
    }


    emitData(key: string, opts: any) {
        if (this._observableEmitter[key]) {
            this._observableEmitter[key].emit(opts);
        }
    }


    getEmitter(key: string): any {
        if (key) {
            this._observableEmitter[key] = new EventEmitter();
            return this._observableEmitter[key];
        }
    }


    private handleError(error: any): any {
        let errJson: any = {};
        try {
            if (error && error.status === 403) {
                errJson = error.json();
                if (errJson && errJson.message === 'Authentication failed') {
                    const cookies = document.cookie.split(';');
                    for (let i = 0; i < cookies.length; i++) {
                        const cookie = cookies[i].split('=')[0].trim();
                        if (cookie.length > 0) {
                            document.cookie = cookie + '=; expires=Thu, 01 Jan 1970 00:00:00 UTC';
                        }
                    }
                    localStorage.clear();
                    window.location.href = window.location.protocol + '//' + window.location.hostname + '/';
                }
            } else {
                return throwError(error.json() || 'Server error');
            }
        } catch (e) {
            return throwError(error || 'Server error');
        }
    }
}


