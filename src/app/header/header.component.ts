import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  @Input() homeButton: boolean = false;
  constructor(private router: Router) { }

  ngOnInit() {

  }

  navigateToHome() {
    this.router.navigate(['home/assessment']);
  }

  logout() {
    this.router.navigate(['']);
  }

  navigateToInstructions() {
    this.router.navigate(['home/landing']);
  }
}