import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApiService } from '../services/api.service';

@Component({
    selector: 'app-numerical-ability',
    templateUrl: './numerical-ability.component.html',
    styleUrls: ['./numerical-ability.component.css']
})
export class NumericalAbilityComponent implements OnInit, OnDestroy {

    public data: any;
    public currentIndex: number = 0;
    public numericalDataLength: any;
    constructor(private router: Router, private apiService: ApiService) {
        const { extras: { state: { data = null } = {} } = {} } = this.router.getCurrentNavigation();
        this.data = data;
    }

    ngOnInit() {
        if (localStorage.getItem('singleselectnumericaldata')) {
            this.data = JSON.parse(localStorage.getItem('singleselectnumericaldata'));
            this.numericalDataLength = JSON.parse(localStorage.getItem('numericaldata')).length;
        }

        this.apiService.getEmitter('navigate-to-na-next').subscribe((result: any) => {
            this.navigate();
        });
    }

    navigate() {
        let numericalData = JSON.parse(localStorage.getItem('numericaldata'));
        let index = localStorage.getItem('numericaldataindex');
        this.currentIndex = parseInt(index) + 1;


        if (this.currentIndex <= this.numericalDataLength - 1) {
            localStorage.setItem('numericaldataindex', '' + this.currentIndex);
            this.apiService.emitData('re-render-ssi', numericalData[this.currentIndex]);
        }

    }

    redirectToHome() {
        localStorage.removeItem('numericaldataindex');
        localStorage.removeItem('singleselectnumericaldata');
        localStorage.removeItem('numericaldata');
        this.router.navigate(['home/assessment']);
    }

    ngOnDestroy() {
        localStorage.removeItem('numericaldataindex');
        localStorage.removeItem('singleselectnumericaldata');
        localStorage.removeItem('numericaldata');
    }
}