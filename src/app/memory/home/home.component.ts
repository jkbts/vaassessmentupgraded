import { Component, ChangeDetectionStrategy, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MemoryService } from '../memory/memory.service';

@Component({
  selector: 'home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
  // changeDetection: ChangeDetectionStrategy.OnPush
})
export class MemoryHomeComponent implements OnInit {

  public view: any = 'new';
  public timerSettings: any;
  constructor(private router: Router, private memory: MemoryService) { }

  ngOnInit() {
    // this.view = 'new';
    // this.memory.newGame();
    this.timerSettings = {
      "maxMinutes": "5",
      "timerType": "countdown",
      "intervalSeconds": "1",
      "timeUpText": "",
      "timerText": "Timeup",
      "question": "MemoryTimer"
    }
  }

  navigateToHome() {
    this.router.navigate(['home/assessment']);
  }

  about() {
    this.view = 'about';
    // this.router.navigate(['home/memory/about']);
  }

  newGame() {
    this.view = 'new';
    // this.router.navigate(['home/momory/new']);
    this.memory.newGame();
  }

  navigateToNextPage() {
    this.router.navigate(['home/memory2']);
  }
}
