import { Component, OnInit } from '@angular/core';
import {MemoryService} from "../memory/memory.service";
import { Router } from '@angular/router';

@Component({
  selector: 'state',
  templateUrl: './state.component.html'
})
export class StateComponent implements OnInit {

  constructor(private memory: MemoryService, private router: Router) { }

  ngOnInit() {
  }

}
