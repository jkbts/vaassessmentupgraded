import {Injectable} from '@angular/core';
import {Card} from "./card.class";
import {Observable} from "rxjs";
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CardsService {

  constructor(private http: HttpClient) {
  }

  getCards(): Observable<any> {
    return this.getFontAwesomeCards();
  }

  getFontAwesomeCards(): Observable<any> {
    return this.http.get(prefixRepo('../../../assets/json/cards.json'))
      .pipe(map((res: Array<any>)=> {
        let icons = res;
        return icons.map(icon => new Card(guid(), this.faTemplate(icon)));
      }));
  }

  faTemplate(icon) {
    return '<i class="fa ' + icon + ' aria-hidden="true"></i>';
  }

}


/** Generate unique ID */
function guid() {
  function s4() {
    return Math.floor((1 + Math.random()) * 0x10000)
      .toString(16)
      .substring(1);
  }

  return s4() + s4() + '-' + s4() + '-' + s4() + '-' +
    s4() + '-' + s4() + s4() + s4();
}

var prefixRepo = (path) => {
  return 'memory' + path;
};
