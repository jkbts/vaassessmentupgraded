import { Component, Input, OnDestroy, OnInit } from '@angular/core';


@Component({
    selector: 'app-timer',
    templateUrl: './timer.component.html',
    styleUrls: ['./timer.component.css']
})

export class TimerComponent implements OnInit, OnDestroy {
    @Input() data: any;

    timerValue: any;

    timerInterval: any;

    timeUp: boolean = false;

    intervalDelay: number = 1000;

    timeUpText: string;

    triggers: any[];

    maxMinutes: number = 0;

    isCountUp: boolean = false;

    timerStarted: boolean = false;

    currentTimerValue: number;

    private newPushedTriggers: Array<any> = [];


    constructor() { }

    ngOnInit() {
        if (this.data && this.data.maxMinutes) {
            this.maxMinutes = Math.floor(parseFloat(this.data.maxMinutes + '') * 60);
            this.isCountUp = this.data.timerType === 'countup' ? true : false;

            if (this.data.intervalSeconds) {
                this.intervalDelay = parseInt(this.data.intervalSeconds + '', null) * 1000;
            }

            if (this.data.timeUpText) {
                this.timeUpText = this.data.timeUpText;
            }

            // this.data.basicTimerSettings.syncInterval = this.data.basicTimerSettings.syncInterval ? parseInt(this.data.basicTimerSettings.syncInterval + '', 10) : 5;
            // this.data.basicTimerSettings.syncTimerKey = this.data.basicTimerSettings.syncTimerKey ? this.data.basicTimerSettings.syncTimerKey : 'PulseTimer';
        }

        this.initTimer();
    }

    startTimer(totalSeconds: number, currentTimerValue?: number): void {
        const self = this;

        let days: any, hoursLeft: any, hours: any, minutesLeft: any, minutes: any,
            seconds = (currentTimerValue !== null && currentTimerValue !== undefined) ? currentTimerValue : totalSeconds,
            remainingSeconds: any, syncNow: boolean = false, syncCounter: number = -1;
        const showHours = Math.floor(seconds / 3600) > 0 ? true : false;
        const showDays = showHours ? (Math.floor(seconds / 24 / 60 / 60) > 0 ? true : false) : false;

        if (this.isCountUp) {
            seconds = (currentTimerValue !== null && currentTimerValue !== undefined) ? currentTimerValue : 0;
        }

        function updateTimer(): void {
            days = Math.floor(seconds / 24 / 60 / 60);
            hoursLeft = !self.isCountUp ? (Math.floor((seconds) - (days * 86400))) : (Math.floor((seconds) + (days * 86400)));
            hours = Math.floor(hoursLeft / 3600);
            minutesLeft = !self.isCountUp ? (Math.floor((hoursLeft) - (hours * 3600))) : (Math.floor((hoursLeft) + (hours * 3600)));
            minutes = Math.floor(minutesLeft / 60);
            remainingSeconds = seconds % 60;
            self.timerValue = '';
            self.timerValue += showDays ? (self.pad(days) + ':') : '';
            self.timerValue += self.pad(hours) + ':';
            self.timerValue += self.pad(minutes) + ':';
            self.timerValue += self.pad(remainingSeconds);

            self.currentTimerValue = seconds;
            if (self.isCountUp) {
                if (seconds === totalSeconds) {
                    self.triggerOnTimeUp();
                } else {
                    seconds += (self.intervalDelay / 1000);
                }
            } else {
                if (seconds === 0) {
                    self.triggerOnTimeUp();
                } else {
                    seconds -= (self.intervalDelay / 1000);
                }
            }
        }

        updateTimer();

        if (!this.timeUp) {
            this.timerInterval = setInterval(() => {
                updateTimer();
            }, this.intervalDelay);
        }
    }

    pad(n: any): any {
        return (n < 10 ? '0' + n : n);
    }

    initTimer(): void {
        if (!this.timerStarted && this.data) {
            this.timerStarted = true;
            this.startTimer(this.maxMinutes, this.currentTimerValue);
        }
    }

    triggerOnTimeUp() {
        this.timeUp = true;
        clearInterval(this.timerInterval);
        // this.pas.clearCacheInterval();
    }

    ngOnDestroy() {
        if (this.timerInterval) {
            clearInterval(this.timerInterval);
        }
    }
}
