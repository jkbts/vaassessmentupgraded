import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  public view: any = 'register';
  public countries: Array<any>;
  public industries: Array<any>;
  public profiles: Array<any>;
  public selectedIndustry: any;
  public selectedCountry: any;
  public selectedProfile: any;
  public profilePhoto: any;
  public profilePhotoName: any;

  constructor(private router: Router, private http: HttpClient) { }

  ngOnInit() {
    this.http.get('../../assets/json/countries.json').subscribe((result: Array<any>) => {
      this.countries = result;
    });

    this.http.get('../../assets/json/industries.json').subscribe((result: Array<any>) => {
      this.industries = result;
    });

    this.http.get('../../assets/json/profiles.json').subscribe((result: Array<any>) => {
      this.profiles = result;
    });
  }

  register() {
    this.view = 'login';
  }

  login() {
      this.router.navigate(['home/landing']);
  }

  selectCountry(country: any) {
    this.selectedCountry = country.name;
  }

  selectIndustry(industry: any) {
    this.selectedIndustry = industry.name;
  }

  selectProfile(profile: any) {
    this.selectedProfile = profile.name;
  }

  getFileName(event) {
		var self = this;
		var fileInput = event.target;
		var filePath = fileInput.value;
		var allowedExtensions = /(\.jpg|\.jpeg|\.png|\.gif)$/i;

		if (!allowedExtensions.exec(filePath)) {
			alert('Please upload file having extensions .jpeg/.jpg/.png/.gif only.');
			fileInput.value = '';
			return false;
		} else {
			if (fileInput.files && fileInput.files[0]) {
				this.profilePhoto = fileInput.files[0];
				this.profilePhotoName = '../../assets/images/' + fileInput.files[0].name;
        /*var reader = new FileReader();
				reader.readAsDataURL(fileInput.files[0]);
				reader.onload = function(event:any) {
					self.coachee.pic = event.target.result;
				};*/
			}
		}
	}
}
