import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-landing',
  templateUrl: './landing.component.html',
  styleUrls: ['./landing.component.css']
})
export class LandingComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    localStorage.setItem('naCompletedStatus', JSON.stringify(0));
    localStorage.setItem('arCompletedStatus', JSON.stringify(0));
  }

  startAssessment() {
      this.router.navigate(['home/assessment']);
  }
}