import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import * as Highcharts from 'highcharts';
// import { HighchartsStatic } from './highchartsstatic';
import * as _ from 'lodash';

@Component({
    selector: 'app-dashboard',
    templateUrl: './dashboard.component.html',
    styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

    @ViewChild('chartelem', { static: true }) chartElem: ElementRef;
    @ViewChild('chartelem1', { static: true }) chartElem1: ElementRef;
    @ViewChild('chartelem2', { static: true }) chartElem2: ElementRef;
    @ViewChild('chartelem3', { static: true }) chartElem3: ElementRef;
    @ViewChild('chartelem4', { static: true }) chartElem4: ElementRef;
    @ViewChild('chartelem5', { static: true }) chartElem5: ElementRef;
    chart: any;

    constructor(private router: Router) {
        const hc = Highcharts,
            hm = require('highcharts/highcharts-more'),
            sg = require('highcharts/modules/solid-gauge'),
            hmap = require('highcharts/modules/heatmap');

        // this._highchartsStatic = hc;

        hm(hc);
        sg(hc);
        hmap(hc);
    }

    ngOnInit() {
        this.makeChart1();
        this.makeChart2();
        this.makeChart3();
        this.makeChart4();
        this.makeChart5();
        this.makeChart6();
    }

    makeChart1() {

        let chartOptions: any = {

            chart: {
                renderTo: this.chartElem.nativeElement,
                type: 'gauge',
                backgroundColor: 'transparent'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            pane: {
                center: ['50%', '85%'],
                size: '120%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#eee',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                plotBands: [{
                    from: 0, 
                    to: 25,
                    color: "#bf1200",
                    innerRadius: "60%"
                }, {
                    from: 25, 
                    to: 75,
                    color: "#f9d91b",
                    innerRadius: "60%"
                }, {
                    from: 75, 
                    to: 100,
                    color: "#6db911",
                    innerRadius: "60%"
                }],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    text: 'Memory Score',
                    y: -60
                },
                labels: {
                    y: 16
                },
                min: 0,
                max: 100
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'Memory Score',
                dataLabels: {
                    enabled: false
                },
                tooltip: {
                    valueSuffix: ' Pending'
                },
                data: [20]
            }]
        };

        let defaults: Highcharts.Options;
        // defaults = this.chartUtils.getDefaultsFor(this.defaultOptions);
        const opt = {};
        _.merge(opt, defaults, chartOptions);
        // console.log(this.chartConfig.get());
        this.chart = Highcharts.chart(chartOptions);
    }

    makeChart2() {

        let chartOptions: any = {

            chart: {
                renderTo: this.chartElem1.nativeElement,
                type: 'gauge',
                backgroundColor: 'transparent'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            pane: {
                center: ['50%', '85%'],
                size: '120%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#eee',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                plotBands: [{
                    from: 0, 
                    to: 25,
                    color: "#bf1200",
                    innerRadius: "60%"
                }, {
                    from: 25, 
                    to: 75,
                    color: "#f9d91b",
                    innerRadius: "60%"
                }, {
                    from: 75, 
                    to: 100,
                    color: "#6db911",
                    innerRadius: "60%"
                }],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    text: 'Numerical Ability Score',
                    y: -60
                },
                labels: {
                    y: 16
                },
                min: 0,
                max: 100
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'Numerical Ability Score',
                dataLabels: {
                    enabled: false
                },
                tooltip: {
                    valueSuffix: ' Pending'
                },
                data: [50]
            }]
        };

        let defaults: Highcharts.Options;
        // defaults = this.chartUtils.getDefaultsFor(this.defaultOptions);
        const opt = {};
        _.merge(opt, defaults, chartOptions);
        // console.log(this.chartConfig.get());
        this.chart = Highcharts.chart(chartOptions);
    }

    makeChart3() {

        let chartOptions: any = {

            chart: {
                renderTo: this.chartElem2.nativeElement,
                type: 'gauge',
                backgroundColor: 'transparent'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            pane: {
                center: ['50%', '85%'],
                size: '120%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#eee',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                plotBands: [{
                    from: 0, 
                    to: 25,
                    color: "#bf1200",
                    innerRadius: "60%"
                }, {
                    from: 25, 
                    to: 75,
                    color: "#f9d91b",
                    innerRadius: "60%"
                }, {
                    from: 75, 
                    to: 100,
                    color: "#6db911",
                    innerRadius: "60%"
                }],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    text: 'Abstract Reasoning Score',
                    y: -60
                },
                labels: {
                    y: 16
                },
                min: 0,
                max: 100
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'Abstract Reasoning Score',
                dataLabels: {
                    enabled: false
                },
                tooltip: {
                    valueSuffix: ' Pending'
                },
                data: [80]
            }]
        };

        let defaults: Highcharts.Options;
        // defaults = this.chartUtils.getDefaultsFor(this.defaultOptions);
        const opt = {};
        _.merge(opt, defaults, chartOptions);
        // console.log(this.chartConfig.get());
        this.chart = Highcharts.chart(chartOptions);
    }

    makeChart4() {

        let chartOptions: any = {

            chart: {
                renderTo: this.chartElem3.nativeElement,
                type: 'gauge',
                backgroundColor: 'transparent'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            pane: {
                center: ['50%', '85%'],
                size: '120%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#eee',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                plotBands: [{
                    from: 0, 
                    to: 25,
                    color: "#bf1200",
                    innerRadius: "60%"
                }, {
                    from: 25, 
                    to: 75,
                    color: "#f9d91b",
                    innerRadius: "60%"
                }, {
                    from: 75, 
                    to: 100,
                    color: "#6db911",
                    innerRadius: "60%"
                }],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    text: 'Problem Solving Score',
                    y: -60
                },
                labels: {
                    y: 16
                },
                min: 0,
                max: 100
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'Problem Solving Score',
                dataLabels: {
                    enabled: false
                },
                tooltip: {
                    valueSuffix: ' Pending'
                },
                data: [0]
            }]
        };

        let defaults: Highcharts.Options;
        // defaults = this.chartUtils.getDefaultsFor(this.defaultOptions);
        const opt = {};
        _.merge(opt, defaults, chartOptions);
        // console.log(this.chartConfig.get());
        this.chart = Highcharts.chart(chartOptions);
    }

    makeChart5() {

        let chartOptions: any = {

            chart: {
                renderTo: this.chartElem4.nativeElement,
                type: 'gauge',
                backgroundColor: 'transparent'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            pane: {
                center: ['50%', '85%'],
                size: '120%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#eee',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                plotBands: [{
                    from: 0, 
                    to: 25,
                    color: "#bf1200",
                    innerRadius: "60%"
                }, {
                    from: 25, 
                    to: 75,
                    color: "#f9d91b",
                    innerRadius: "60%"
                }, {
                    from: 75, 
                    to: 100,
                    color: "#6db911",
                    innerRadius: "60%"
                }],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    text: 'Critical Reasoning Score',
                    y: -60
                },
                labels: {
                    y: 16
                },
                min: 0,
                max: 100
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'Critical Reasoning Score',
                dataLabels: {
                    enabled: false
                },
                tooltip: {
                    valueSuffix: ' Pending'
                },
                data: [0]
            }]
        };

        let defaults: Highcharts.Options;
        // defaults = this.chartUtils.getDefaultsFor(this.defaultOptions);
        const opt = {};
        _.merge(opt, defaults, chartOptions);
        // console.log(this.chartConfig.get());
        this.chart = Highcharts.chart(chartOptions);
    }

    makeChart6() {

        let chartOptions: any = {

            chart: {
                renderTo: this.chartElem5.nativeElement,
                type: 'gauge',
                backgroundColor: 'transparent'
            },
            title: {
                text: ''
            },
            credits: {
                enabled: false
            },
            tooltip: {
                enabled: false
            },
            pane: {
                center: ['50%', '85%'],
                size: '120%',
                startAngle: -90,
                endAngle: 90,
                background: {
                    backgroundColor: '#eee',
                    innerRadius: '60%',
                    outerRadius: '100%',
                    shape: 'arc'
                }
            },
            yAxis: {
                stops: [
                    [0.1, '#55BF3B'], // green
                    [0.5, '#DDDF0D'], // yellow
                    [0.9, '#DF5353'] // red
                ],
                plotBands: [{
                    from: 0, 
                    to: 25,
                    color: "#bf1200",
                    innerRadius: "60%"
                }, {
                    from: 25, 
                    to: 75,
                    color: "#f9d91b",
                    innerRadius: "60%"
                }, {
                    from: 75, 
                    to: 100,
                    color: "#6db911",
                    innerRadius: "60%"
                }],
                lineWidth: 0,
                minorTickInterval: null,
                tickAmount: 2,
                title: {
                    text: 'Decision Making Score',
                    y: -60
                },
                labels: {
                    y: 16
                },
                min: 0,
                max: 100
            },
            plotOptions: {
                solidgauge: {
                    dataLabels: {
                        y: 5,
                        enabled: false
                    }
                }
            },
            series: [{
                name: 'Decision Making Score',
                dataLabels: {
                    enabled: false
                },
                tooltip: {
                    valueSuffix: ' Pending'
                },
                data: [0]
            }]
        };

        let defaults: Highcharts.Options;
        // defaults = this.chartUtils.getDefaultsFor(this.defaultOptions);
        const opt = {};
        _.merge(opt, defaults, chartOptions);
        // console.log(this.chartConfig.get());
        this.chart = Highcharts.chart(chartOptions);
    }

    navigateToRecommendations() {
        this.router.navigate(['home/recommendations']);
    }
}