import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { ApiService } from '../services/api.service';

@Component({
    selector: 'app-abstract-reasoning',
    templateUrl: './abstract-reasoning.component.html',
    styleUrls: ['./abstract-reasoning.component.css']
})
export class AbstractReasoningComponent implements OnInit, OnDestroy {

    public data: any;
    public currentIndex: number = 0;
    public abstractDataLength: any;
    constructor(private router: Router, private apiService: ApiService) {
        const { extras: { state: { data = null } = {} } = {} } = this.router.getCurrentNavigation();
        this.data = data;
    }

    ngOnInit() {
        if (localStorage.getItem('singleselectdata')) {
            this.data = JSON.parse(localStorage.getItem('singleselectdata'));
            this.abstractDataLength = JSON.parse(localStorage.getItem('abstractdata')).length;
        }

        this.apiService.getEmitter('navigate-to-ar-next').subscribe((result: any) => {
            this.navigate();
        });
    }

    navigate() {
        let abstractData = JSON.parse(localStorage.getItem('abstractdata'));
        let index = localStorage.getItem('abstractdataindex');
        this.currentIndex = parseInt(index) + 1;
        if (this.currentIndex <= this.abstractDataLength - 1) {
            localStorage.setItem('abstractdataindex', '' + this.currentIndex);
            this.apiService.emitData('re-render-ssi', abstractData[this.currentIndex]);
        }
    }

    redirectToHome() {
        localStorage.removeItem('abstractdataindex');
        localStorage.removeItem('singleselectdata');
        localStorage.removeItem('abstractdata');
        this.router.navigate(['home/assessment']);
    }

    ngOnDestroy() {
        localStorage.removeItem('abstractdataindex');
        localStorage.removeItem('singleselectdata');
        localStorage.removeItem('abstractdata');
    }
}